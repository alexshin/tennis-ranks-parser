<?php

class AtpRank extends Rank {
    public function getPlayersList(){
        $html = $this->getHtml();

        if (!$table = $html->find('div[id=rankingDetailAjaxContainer] table[class=mega-table]', 0)){
            throw new PlayerParserException('Needle table is not exist');
        }

        if (!$body = $table->children(1)){
            throw new PlayerParserException('Table has not a body "tbody"');
        }

        $players = [];
        foreach ($body->children() as $row){
            $currentRank = (int) $this->cleanUpRank($row->children(0)->text());
            $name = (string) $row->children(3)->text();
            $scores = (int) $this->cleanUpScores($row->children(5)->text());
            $offsetDirection = (int) $this->getDirection($row->children(1)->children(0)->class);
            if ($checkOffset = $row->children(1)->children(1)->text()){
                $offset = (int) $checkOffset;
            }
            else {
                $offset = 0;
            }

            $players[] = new Player(
                $currentRank,
                trim($name),
                $scores,
                $offsetDirection,
                $offset * $offsetDirection
            );
        }

        return $players;
    }

    private function cleanUpRank($string){
        return (int) preg_replace('/(\d+).+/i', '$1', $string);
    }

    private function cleanUpScores($string){
        return (int) preg_replace('/(\d+),(\d+)/i', '$1$2', $string);
    }

    private function getDirection($string){
        switch ($string) {
            case 'move-none':
                return 0;
            case 'move-up':
                return 1;
            case 'move-down':
                return -1;
        }
    }



}