<?php

abstract class Rank {

    /** @var  string */
    protected $uri;

    /** @var  simple_html_dom */
    protected $html;

    /**
     * @param string $uri
     */
    public function __construct($uri){
        $this->uri = $uri;

        /* It is a dirty hack for Bitrix support =( */
        require_once 'simple_html_dom/simple_html_dom.php';

        $this->setHtml($uri);
    }

    /**
     * If uri was changed
     */
    public function reload(){
        $this->setHtml($this->uri);
    }

    /**
     * @param string $uri
     */
    protected function setHtml($uri){
        $this->html = file_get_html($uri);
    }

    /**
     * @return simple_html_dom
     */
    public function getHtml(){
        return $this->html;
    }

    /**
     * @return Player[]|bool
     * @trows PlayerParserException
     */
    public abstract function getPlayersList();
}


/**
 * Class Player
 */
class Player {

    /** @var  int */
    private $rankNumber;

    /** @var  string */
    private $name;

    /** @var  int */
    private $scores;

    /** @var  int -1 | 0 | +1 */
    private $offsetDirection;

    /** @var  int */
    private $offset;

    /**
     * Sportsman constructor.
     * @param int $rankNumber
     * @param string $name
     * @param int $scores
     * @param int $offsetDirection
     * @param int $offset
     */
    public function __construct($rankNumber, $name, $scores, $offsetDirection, $offset = null)
    {
        $this->rankNumber = $rankNumber;
        $this->name = $name;
        $this->scores = $scores;
        $this->offset = $offset;
        $this->offsetDirection = $offsetDirection;
    }

    /**
     * @return int
     */
    public function getOffsetDirection()
    {
        return $this->offsetDirection;
    }

    /**
     * @return int
     */
    public function getRankNumber()
    {
        return $this->rankNumber;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getScores()
    {
        return $this->scores;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }


}


class PlayerParserException extends Exception {}