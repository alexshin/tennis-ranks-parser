<?php

/**
 * Class RankFactory
 * Build class for one of needle parser
 */
final class RankFactory {

    /**
     * @param string $rankType
     * @param string $uri
     * @return Rank
     *
     * @throws RankTypeException
     */
    public static function build($rankType, $uri){
        $accessRanks = ['Itf', 'Wta', 'Atp', 'Te'];

        if (!in_array($rankType, $accessRanks, true)){
            throw new RankTypeException('Rank is not in allowed for this factory');
        }

        $class = $rankType.'Rank';
        require_once $class.'.php';

        if (!class_exists($class)){
            throw new RankTypeException('The needle class '.$class.' is not exist');
        }

        return new $class($uri);
    }
}


class RankTypeException extends Exception {}