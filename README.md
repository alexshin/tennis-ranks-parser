# README #

The project contains PHP library for getting ranks from tennis sites:

* Tennis Europe
* ATP
* ITF
* WTA

You can use that very simple:


```
#!php

<?php
require_once "RankFactory.php";
require_once "Rank.php";

$factory = RankFactory::build('Atp', 'http://www.atpworldtour.com/en/rankings/[some-url]');
$players = $factory->getPlayersList();
```

## Player ##

Player structure consists of:

* rankNumber (method getRankNumber) - current rank
* name (method getName) - the player name
* scores (method getScores) - scores of the player
* offsetDirection (method getOffsetDirection) - could be: -1, 0 or 1
* offset (method getOffset) - offset relative to current rank