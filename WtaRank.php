<?php

class WtaRank extends Rank {


    public function getPlayersList() {
        $html = $this->getHtml();

        if (!$table = $html->find('table[id=myTable]', 0)){
            throw new PlayerParserException('Needle table is not exist');
        }

        if (!$body = $table->children(1)){
            throw new PlayerParserException('Table has not a body "tbody"');
        }

        $players = [];
        foreach ($body->children() as $row){

            $currentRank = (int) $row->children(1)->text();
            $name = (string) $row->children(2)->text();
            $scores = (int) $row->children(5)->text();
            $offsetDirection = $this->getDirection((string) $row->children(0)->children(0)->class);
            $offset = $this->getOffset($currentRank, (int) $this->cleanUpPreviousRank($row->children(0)->text()));

            $players[] = new Player(
                $currentRank,
                $name,
                $scores,
                $offsetDirection,
                $offset
            );
        }

        return $players;


    }


    private function cleanUpPreviousRank($string){
        return (int) preg_replace('/\[(\d+)\]/i', '$1', $string);
    }


    private function getDirection($string){
        switch ($string) {
            case 'no-arrow':
                return 0;
            case 'up-arrow':
                return 1;
            case 'down-arrow':
                return -1;
        }
    }


    private function getOffset($currentRank, $previousRank){
        return (int) $previousRank - (int) $currentRank;
    }


}